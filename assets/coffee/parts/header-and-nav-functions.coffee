###======================================================================================
=            Scroll Header Background and Fix Nav Position on Window Scrool             =
=====================================================================================####
do ->
  window.onscroll = ->
    nav                 = document.querySelector('nav')
    header              = document.querySelector('header')
    header_height       = header.offsetHeight
    window_top_position = window.pageYOffset

    if window_top_position > header_height
      # set nav position to fixed if window top position pass the header
      nav.classList.add 'fixed'
      header.classList.add 'fixed'

    else
      # change header background position
      header.style.backgroundPosition = '50% ' + window_top_position + 'px'

      # remove fixed position when window scroll reaches the header
      nav.classList.remove 'fixed'
      header.classList.remove 'fixed'