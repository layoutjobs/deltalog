###======================================================================================
=                                Animate Window Scroll                                  =
======================================================================================###
do ->
  nav_a = document.querySelectorAll('nav a')
  to_top = document.querySelector('.icon-top_arrow')

  to_top.onclick = ->
    $('html, body').stop().animate { scrollTop: 0 }, 1000

  i = 0
  while i < nav_a.length
    
    nav_a[i].onclick = ->
      
      section = @getAttribute('data-section')
      goTo = document.querySelector(section).offsetTop

      $('html, body').stop().animate { scrollTop: goTo }, 1000

      return false

    i++