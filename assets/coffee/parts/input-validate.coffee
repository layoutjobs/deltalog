###====================================================
=            Validate Text Inputs Function            =
====================================================###

text_input = document.querySelectorAll('form .text')

i = 0
while i < text_input.length
  text_input[i].onblur = ->
    content = @value
    content = content.replace(RegExp('    ', 'g'), ' ') #trim four blank spaces
    content = content.replace(RegExp('   ', 'g'), ' ') #trim three blank spaces
    content = content.replace(RegExp('  ', 'g'), ' ') #trim two blank spaces
    type    = @getAttribute('type')

    
    # Replace content and value to empty if there are just blank spaces
    if content == ' '
      @value = ''
      content = ''
    else
      @value = content


    # If content is not empty fix label
    if content != ''
      @nextElementSibling.classList.add 'fixed'
    else
      @nextElementSibling.classList.remove 'fixed'


    # If type equals email verify if email exists
    if type == 'email'
      form     = @getAttribute('data-form')
      submit   = document.querySelector(form + ' .submit')
      data     = 'domain=' + @value.split('@')[1]
      was_sent = submit.getAttribute 'data-send'

      if was_sent != 'true'
        request = new XMLHttpRequest
        request.open 'POST', 'inc/validate-email.php', true
        request.setRequestHeader 'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8'
        request.send data

        request.onload = ->
          if request.status >= 200 and request.status < 400
            response = request.responseText
            
            if response != 'valid-email'
              submit.setAttribute 'disabled', 'true'
              submit.classList.add 'alert'
              submit.innerHTML = 'Informe um email válido'
            else
              submit.removeAttribute 'disabled'
              submit.classList.remove 'alert'
              submit.innerHTML = 'Enviar'
  i++