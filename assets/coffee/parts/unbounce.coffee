###=====================================================================
=            Show modal form when user are leaving the page            =
=====================================================================###

do ->
  modal_form = document.querySelector('.modal.form')
  modal_bg = document.querySelector('.modal.background')

  document.querySelector('body').addEventListener 'mouseleave', (element) ->
    displayed = modal_form.getAttribute('data-displayed')

    if element.offsetY - $(window).scrollTop() < 0 and displayed != 'true'

      modal_form.style.display = 'block'
      modal_bg.style.display = 'block'

      setTimeout (->
        modal_bg.classList.add 'show'
        modal_form.classList.add 'show'
        modal_form.classList.add 'shake'
        modal_form.setAttribute 'data-displayed', 'true'
      ),500