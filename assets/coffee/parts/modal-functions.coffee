###===============================================================
=            Show Next Image Banner On Modal Window            =
===============================================================###
do ->
  document.querySelector('.modal .icon-right_arrow').onclick = ->
    img     = document.querySelector('.modal img')
    caption = document.querySelector('.modal figcaption')
    group   = img.getAttribute('data-group')
    section = img.getAttribute('data-section')
    next    = img.getAttribute('data-number')
    next    = parseInt(next) + 1
    
    if next > 3 then (next = 1) else (next = next)

    next_caption = document.querySelector(section + ' .figure' + next).getAttribute('data-caption')

    caption.innerHTML = next_caption
    caption.style.display = 'block'
    img.classList.add 'hide'
    
    setTimeout (->
      img.setAttribute 'src', 'assets/img/' + group + '-0' + next + '.jpg'
      img.setAttribute 'data-number', next
      img.classList.remove 'hide'
      return
    ), 500






###===============================================================
=            Show Previous Image Banner On Modal Window          =
===============================================================###
do ->
  document.querySelector('.modal .icon-left_arrow').onclick = ->
    img     = document.querySelector('.modal img')
    caption = document.querySelector('.modal figcaption')
    group   = img.getAttribute('data-group')
    section = img.getAttribute('data-section')
    next    = img.getAttribute('data-number')
    next    = parseInt(next) - 1
    
    if next < 1 then (next = 3) else (next = next)

    next_caption = document.querySelector(section + ' .figure' + next).getAttribute('data-caption')
    
    caption.innerHTML = next_caption
    caption.style.display = 'block'
    img.classList.add 'hide'
    
    setTimeout (->
      img.setAttribute 'src', 'assets/img/' + group + '-0' + next + '.jpg'
      img.setAttribute 'data-number', next
      img.classList.remove 'hide'
      return
    ), 500
    return





###==========================================
=            Close Modal Window            =
==========================================###
do ->
  document.querySelector('.modal.background').onclick = ->

    # select all modal windows
    modal_form = document.querySelector('.modal.form')
    modal_table = document.querySelector('.modal.table')
    modal_banner = document.querySelector('.modal.banner')
    modal_bg = document.querySelector('.modal.background')

    # remove 'show' class from all modal windows
    modal_form.classList.remove 'show'
    modal_table.classList.remove 'show'
    modal_banner.classList.remove 'show'
    modal_bg.classList.remove 'show'

    # wait one second till the css animation ends and apply display none on all modal windows
    setTimeout (->
      modal_form.classList.remove 'shake'
      modal_form.style.display = 'none'
      modal_table.style.display = 'none'
      modal_banner.style.display = 'none'
      modal_bg.style.display = 'none'
      return
    ), 1000