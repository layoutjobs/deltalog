###========================================================
=            Magnify Images On a Modal Banner             =
========================================================###
do ->
  $('.icon-magnify, .icon-magnify + img, figure .img, .aerial_view button').on 'click', ->
    img                  = @parentNode.getAttribute('data-image')
    group                = @parentNode.getAttribute('data-group')
    number               = @parentNode.getAttribute('data-number')
    caption              = @parentNode.getAttribute('data-caption')
    section              = @parentNode.getAttribute('data-section')
    modal_banner         = document.querySelector('.modal.banner')
    modal_backgrund      = document.querySelector('.modal.background')
    img_placeholder      = document.querySelector('.modal figure img')
    caption_placeholder  = document.querySelector('.modal figure figcaption')
    
    modal_banner.style.display = 'block'
    modal_backgrund.style.display = 'block'
    
    setTimeout (->
      modal_banner.classList.add 'show'
      modal_backgrund.classList.add 'show'
    ), 100
    
    img_placeholder.setAttribute 'src', 'assets/img/' + img
    img_placeholder.setAttribute 'data-group', group
    img_placeholder.setAttribute 'data-number', number
    img_placeholder.setAttribute 'data-section', section
    
    if caption != null
      caption_placeholder.innerHTML = caption
      caption_placeholder.style.display = 'block'
    else
      caption_placeholder.innerHTML = ''
      caption_placeholder.style.display = 'none'