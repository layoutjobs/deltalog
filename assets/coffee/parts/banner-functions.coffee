###======================================================================================
=                              Change Banner Function                                   =
======================================================================================###
banner              = document.querySelector('#prime-logistics > .banner')
banner_left_arrow   = document.querySelector('#prime-logistics > .banner .icon-left_arrow')
banner_right_arrow  = document.querySelector('#prime-logistics > .banner .icon-right_arrow')
banner_thumbs       = document.querySelectorAll('.thumbs > button')

changeBanner = (element) ->
  next_banner   = element.getAttribute('data-number')
  next_caption  = element.getAttribute('data-caption')
  caption       = document.querySelector('#prime-logistics .banner .caption p')
  
  i = 0 
  #pass trough banner_thumbs removing 'active' class
  while i < banner_thumbs.length
    banner_thumbs[i].classList.remove 'active'
    i++
  
  element.classList.add 'active'                      # add 'active' class on clicked thumb
  banner.style.opacity = '0'                          # hide banner
  banner.setAttribute 'data-number', next_banner      # change image
  caption.innerHTML = next_caption                    #change caption
  banner.style.opacity = '1'                          # show banner





###====================================
=            Banner Timer            =
====================================###

thumb = 1
timer = setInterval((->
  thumb++
  
  if thumb > 3 then (thumb = 1) else (thumb = thumb)
  
  document.querySelector('#thumb0' + thumb).click()

), 4000)





###=================================================================
=            Apply changeBanner() on all banner_thumbs             =
=================================================================###

i = 0
while i < banner_thumbs.length
  banner_thumbs[i].addEventListener 'click', (->

    changeBanner this
    clearInterval timer

    timer = setInterval((->
      thumb++
      if thumb > 3 then (thumb = 1) else (thumb = thumb)
      $('#thumb0' + thumb).click()
    ), 4000)

  ), false
  i++





###=================================================
=            Banner Left Arrow Function            =
=================================================###

banner_left_arrow.onclick = ->
  next_banner = banner.getAttribute('data-number')
  next_banner = parseInt(next_banner) - 1

  if next_banner < 1 then (next_banner = 3) else (next_banner = next_banner)

  # banner.setAttribute 'data-number', next_banner
  document.querySelector('#thumb0' + next_banner).click()





###=================================================
=            Banner Right Arrow Function            =
=================================================###

banner_right_arrow.onclick = ->
  next_banner = banner.getAttribute('data-number')
  next_banner = parseInt(next_banner) + 1
  
  if next_banner > 3 then (next_banner = 1) else (next_banner = next_banner)

  # banner.setAttribute 'data-number', next_banner
  document.querySelector('#thumb0' + next_banner).click()