###======================================================================================
=                             Show and Hide Responsive Nav                              =
======================================================================================###

nav = document.querySelector('nav')
nav_a = document.querySelectorAll('nav a')

# show hidden nav
document.querySelector('nav .button').onclick = ->
 document.querySelector('nav').classList.add 'active'
 return

# hide nav when user select an option
i = 0
while i < nav_a.length
 nav_a[i].addEventListener 'click', (->
   nav.classList.remove 'active'
   return
 ), false
 i++